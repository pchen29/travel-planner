package AttractionObjects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SubCategory {
    private SubtypeObject label1;
    private SubtypeObject label2;
    private SubtypeObject label3;
    private SubtypeObject label4;
    private SubtypeObject label5;
    private SubtypeObject label6;
    private SubtypeObject label7;
    private SubtypeObject label8;
    private SubtypeObject label9;
    private SubtypeObject label10;
    private SubtypeObject label11;
    private SubtypeObject label12;
    private SubtypeObject label13;
    private SubtypeObject label14;
    private SubtypeObject label15;
    private SubtypeObject label16;
    private SubtypeObject label17;
    private SubtypeObject label18;
    private SubtypeObject label19;
    private SubtypeObject label20;
@JsonCreator
    public SubCategory(@JsonProperty("0") SubtypeObject label1,
                       @JsonProperty("20")SubtypeObject label2,
                       @JsonProperty("26")SubtypeObject label3,
                       @JsonProperty("36")SubtypeObject label4,
                       @JsonProperty("40")SubtypeObject label5,
                       @JsonProperty("41")SubtypeObject label6,
                       @JsonProperty("42")SubtypeObject label7,
                       @JsonProperty("47")SubtypeObject label8,
                       @JsonProperty("48")SubtypeObject label9,
                       @JsonProperty("49")SubtypeObject label10,
                       @JsonProperty("52")SubtypeObject label11,
                       @JsonProperty("53")SubtypeObject label12,
                       @JsonProperty("55")SubtypeObject label13,
                       @JsonProperty("56")SubtypeObject label14,
                       @JsonProperty("57")SubtypeObject label15,
                       @JsonProperty("58")SubtypeObject label16,
                       @JsonProperty("59")SubtypeObject label17,
                       @JsonProperty("60")SubtypeObject label18,
                       @JsonProperty("61")SubtypeObject label19,
                       @JsonProperty("62")SubtypeObject label20) {
        this.label1 = label1;
        this.label2 = label2;
        this.label3 = label3;
        this.label4 = label4;
        this.label5 = label5;
        this.label6 = label6;
        this.label7 = label7;
        this.label8 = label8;
        this.label9 = label9;
        this.label10 = label10;
        this.label11 = label11;
        this.label12 = label12;
        this.label13 = label13;
        this.label14 = label14;
        this.label15 = label15;
        this.label16 = label16;
        this.label17 = label17;
        this.label18 = label18;
        this.label19 = label19;
        this.label20 = label20;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                label1 +"\n"+
                label2 +"\n"+
                label3 +"\n"+
                label4 +"\n"+
                label5 +"\n"+
                label6 +"\n"+
                label7 +"\n"+
                label8 +"\n"+
                label9 +"\n"+
                label10 +"\n"+
                label11 +"\n"+
                label12 +"\n"+
                label13 +"\n"+
                label14 +"\n"+
                label15 +"\n"+
                label16 +"\n"+
                label17 +"\n"+
                label18 +"\n"+
                label19 +"\n"+
                label20 +"\n"+
                '}';
    }

    public SubtypeObject getLabel1() {
        return label1;
    }

    public SubtypeObject getLabel2() {
        return label2;
    }

    public SubtypeObject getLabel3() {
        return label3;
    }

    public SubtypeObject getLabel4() {
        return label4;
    }

    public SubtypeObject getLabel5() {
        return label5;
    }

    public SubtypeObject getLabel6() {
        return label6;
    }

    public SubtypeObject getLabel7() {
        return label7;
    }

    public SubtypeObject getLabel8() {
        return label8;
    }

    public SubtypeObject getLabel9() {
        return label9;
    }

    public SubtypeObject getLabel10() {
        return label10;
    }

    public SubtypeObject getLabel11() {
        return label11;
    }

    public SubtypeObject getLabel12() {
        return label12;
    }

    public SubtypeObject getLabel13() {
        return label13;
    }

    public SubtypeObject getLabel14() {
        return label14;
    }

    public SubtypeObject getLabel15() {
        return label15;
    }

    public SubtypeObject getLabel16() {
        return label16;
    }

    public SubtypeObject getLabel17() {
        return label17;
    }

    public SubtypeObject getLabel18() {
        return label18;
    }

    public SubtypeObject getLabel19() {
        return label19;
    }

    public SubtypeObject getLabel20() {
        return label20;
    }


}
